import './App.css';
import MatchCarousel from './components/MatchCarousel';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import styled from 'styled-components';
import { useState } from 'react';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;

  width: 100%;
  max-width: 1200px;
  height: 100%;
  padding: 30px;
  margin: auto;
`;

const ButtonGroup = styled.div`
  display: flex;
`;

const Button = styled.button`
  border: none;
  padding: 12px 24px;
  background: #282c34;
  color: white;
  margin-left: 10px;
  margin-right: 10px;
  font-weight: bold;
  border-radius: 8px;
`;

function App() {
  const [isFirstTab, setIsFirstTab] = useState(true);

  return (
    <Wrapper>
      <ButtonGroup>
        <Button onClick={() => setIsFirstTab(true)}>max = 10</Button>
        <Button onClick={() => setIsFirstTab(false)}>different sports</Button>
      </ButtonGroup>
      {isFirstTab ? (
        <MatchCarousel max={10} />
      ) : (
        <>
          <MatchCarousel max={5} sportId={1} />
          <MatchCarousel max={5} sportId={2} />
        </>
      )}
    </Wrapper>
  );
}

export default App;
