import { formatMatch, getMatches, getStage } from '../utils/matchUtils';
import { Stages } from '../match-types';
import { ApiResponse } from '../api-types';
import { mockApiResponse } from '../__mocks__/apiResponse.mock';

describe('getStage', () => {
  it('should return PREMATCH for stageCode 0', () => {
    expect(getStage(0)).toBe(Stages.PREMATCH);
  });

  it('should return POSTMATCH for stageCode 100', () => {
    expect(getStage(100)).toBe(Stages.POSTMATCH);
  });

  it('should return LIVE for any other stageCode', () => {
    expect(getStage(50)).toBe(Stages.LIVE);
  });
});

describe('getMatches', () => {
  it('should retrieve matches for a given sportId', () => {
    const expected = [
      {
        awayTeam: {
          abbr: 'CAC',
          flagURL: 'http://img.sportradar.com/ls/crest/big/3214.png',
          name: 'Chacarita Jrs',
          points: 0,
        },
        homeTeam: {
          abbr: 'BDA',
          flagURL: 'http://img.sportradar.com/ls/crest/big/107201.png',
          name: 'Brown de Adrogue',
          points: 0,
        },
        metadata: { date: '02/10/23', stage: 'NOT STARTED', time: '18:00' },
        title: { main: 'Primera Nacional, Group B – Group stage', subtitle: '' },
      },
    ];
    const result = getMatches(mockApiResponse as unknown as ApiResponse, 1);
    expect(result).toEqual(expected);
  });
});

describe('formatMatch', () => {
  it('should format raw match data correctly', () => {
    const rawMatch = {
      teams: {
        home: { name: 'Team A', abbr: 'A', uid: '123' },
        away: { name: 'Team B', abbr: 'B', uid: '456' },
      },
      result: { home: 1, away: 2 },
      _dt: { time: '10:00', date: '2023-01-01' },
      status: { _id: 0 },
    };

    const expected = {
      title: { main: 'Mock Tournament – Mock Season', subtitle: 'Mock Category' },
      homeTeam: {
        name: 'Team A',
        abbr: 'A',
        flagURL: 'http://img.sportradar.com/ls/crest/big/123.png',
        points: 1,
      },
      awayTeam: {
        name: 'Team B',
        abbr: 'B',
        flagURL: 'http://img.sportradar.com/ls/crest/big/456.png',
        points: 2,
      },
      metadata: {
        time: '10:00',
        date: '2023-01-01',
        stage: Stages.PREMATCH,
      },
    };

    const result = formatMatch(rawMatch, 'Mock Tournament – Mock Season', 'Mock Category');
    expect(result).toEqual(expected);
  });
});
