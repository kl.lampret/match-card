import { useCallback, useEffect, useRef, useState } from 'react';

function useInterval(callback: () => void, time: number | null) {
  const savedCallback = useRef<() => void>();
  const [resetTime, setResetTime] = useState(() => Date.now());

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    const tick = () => {
      if (savedCallback.current) savedCallback.current();
    };

    if (time !== null) {
      const id = setInterval(tick, time);
      return () => clearInterval(id);
    }
  }, [time, resetTime]);

  return useCallback(() => setResetTime(Date.now()), []);
}

export default useInterval;
