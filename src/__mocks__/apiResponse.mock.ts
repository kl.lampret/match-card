import exp from 'constants';
export const mockApiResponse = {
  doc: [
    {
      data: [
        {
          _id: 1,
          realcategories: [
            {
              tournaments: [
                {
                  name: 'Primera Nacional, Group B',
                  seasontypename: 'Group stage',
                  matches: [
                    {
                      _dt: {
                        time: '18:00',
                        date: '02/10/23',
                      },
                      result: {
                        home: null,
                        away: null,
                        winner: null,
                      },
                      teams: {
                        home: {
                          uid: 107201,
                          name: 'Brown de Adrogue',
                          abbr: 'BDA',
                        },
                        away: {
                          uid: 3214,
                          name: 'Chacarita Jrs',
                          abbr: 'CAC',
                        },
                      },
                      status: {
                        _id: 0,
                      },
                    },
                  ],
                },
              ],
            },
          ],
        },
      ],
    },
  ],
};
