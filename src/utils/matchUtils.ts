import { MatchData, Stages } from '../match-types';
import { ApiResponse, Match } from '../api-types';

export function getStage(stageCode: number) {
  if (stageCode === 0) return Stages.PREMATCH;
  if (stageCode === 100) return Stages.POSTMATCH;
  return Stages.LIVE;
}

export function getMatches(
  data: ApiResponse | undefined,
  sportId: number | undefined,
): MatchData[] | undefined {
  return data?.doc[0]?.data
    .filter((sport) => (sportId ? sport._id === sportId : true))
    .flatMap((sport) => sport.realcategories)
    .flatMap((realcategory) =>
      realcategory.tournaments.map((tournament) => ({
        ...tournament,
        subtitle: realcategory.name,
      })),
    )
    .flatMap((tournament) =>
      tournament.matches.map((match) =>
        formatMatch(
          match,
          `${tournament.name} – ${tournament.seasontypename}`,
          tournament.subtitle,
        ),
      ),
    );
}

export function formatMatch(rawMatch: Match, title: string, subtitle: string): MatchData {
  return {
    title: { main: title ?? '', subtitle: subtitle ?? '' },
    homeTeam: {
      name: rawMatch.teams.home.name,
      abbr: rawMatch.teams.home.abbr,
      flagURL: `http://img.sportradar.com/ls/crest/big/${rawMatch.teams.home.uid}.png`,
      points: rawMatch.result.home ?? 0,
    },
    awayTeam: {
      name: rawMatch.teams.away.name,
      abbr: rawMatch.teams.away.abbr,
      flagURL: `http://img.sportradar.com/ls/crest/big/${rawMatch.teams.away.uid}.png`,
      points: rawMatch.result.away ?? 0,
    },
    metadata: {
      time: rawMatch._dt.time,
      date: rawMatch._dt.date,
      stage: getStage(rawMatch.status._id),
    },
  };
}
