import { ApiResponse } from '../api-types';
import { useQuery } from '@tanstack/react-query';

async function getMatchData() {
  const res = await fetch(
    'https://lmt.fn.sportradar.com/demolmt/en/Etc:UTC/gismo/event_fullfeed/0/1/12074',
  );
  return (await res.json()) as ApiResponse;
}

export function useMatches() {
  return useQuery(['matchData'], getMatchData);
}
