export enum Stages {
  LIVE = 'LIVE',
  PREMATCH = 'NOT STARTED',
  POSTMATCH = 'ENDED',
}

export interface Team {
  name: string;
  abbr: string;
  flagURL: string;
  points: number;
}

export interface MatchData {
  title: { main: string; subtitle: string };
  homeTeam: Team;
  awayTeam: Team;
  metadata: { time: string; date: string; stage: Stages };
}
