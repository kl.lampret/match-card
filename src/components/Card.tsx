import React from 'react';
import styled from 'styled-components';
import live from '../assets/bg-live.jpg';
import prematch from '../assets/bg-prematch.jpg';
import postmatch from '../assets/bg-postmatch.jpg';
import { MatchData, Stages } from '../match-types';
import { UiCard } from './UiCard';

const CardContainer = styled(UiCard)<{ background?: any }>`
  width: calc(100% - 20px);
  height: 450px;
  position: relative;
  box-sizing: border-box;

  display: grid;
  grid-template-areas:
    'title title title'
    'home  score  away'
    'stage stage stage';
  gap: 20px;

  /* minmax because of a grid blowout */
  grid-template-rows: minmax(0, 1fr) minmax(0, 2fr) minmax(0, 1fr);
  grid-template-columns: minmax(0, 2fr) minmax(0, 1fr) minmax(0, 2fr);

  margin: 20px 10px;
  padding: 20px;

  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
  border-radius: 16px;

  background: rgba(0, 0, 0, 0.7) url(${(props) => props.background});
  background-size: cover;
  background-position: center;
  background-blend-mode: darken;

  color: white;

  @media only screen and (max-width: 450px) {
    height: 250px;
    margin: 10px 5px;
    padding: 10px;

    border-radius: 8px;
  }

  @media only screen and (max-width: 320px) {
    height: 170px;
    margin: 7px 3px;
    padding: 7px;

    border-radius: 5px;
  }
`;

const Title = styled.div`
  grid-area: title;
  height: 100%;
  width: 100%;
  text-align: center;

  h1 {
    font-size: min(2.3rem, 7vw);
    font-weight: lighter;
  }

  h2 {
    font-size: min(1.5rem, 4.5vw);
    font-weight: lighter;
  }

  @media only screen and (max-width: 450px) {
    h1 {
      font-size: min(1.7rem, 4vw);
    }

    h2 {
      font-size: min(0.7rem, 2vw);
    }
  }

  @media only screen and (max-width: 320px) {
    h1 {
      font-size: min(1.1rem, 3vw);
    }

    h2 {
      font-size: min(0.5rem, 1.5vw);
    }
  }
`;

const Team = styled.div<{ isHomeTeam?: boolean }>`
  grid-area: ${(props) => (props.isHomeTeam ? 'home' : 'away')};
  height: 100%;
  width: 100%;
  padding-top: 40px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;

  img {
    height: 70%;
    max-height: 300px;
  }

  p {
    max-width: 100%;
    font-size: min(2rem, 6vw);
    padding: 0;
    margin: 0;
    font-weight: lighter;
    text-align: center;
  }

  .sm-hide {
    display: block;
  }

  .sm-show {
    display: none;
  }

  @media only screen and (max-width: 450px) {
    padding-top: 20px;
    p {
      font-size: min(1rem, 3vw);
    }
  }

  @media only screen and (max-width: 320px) {
    padding-top: 0;
    p {
      font-size: 0.875rem;
    }

    .sm-hide {
      display: none;
    }

    .sm-show {
      display: block;
    }
  }
`;

const Score = styled.div`
  grid-area: score;
  height: 100%;
  width: 100%;
  font-size: min(5rem, 15vw);
  display: grid;
  place-items: center;
`;

const Time = styled.div`
  grid-area: score;
  height: 100%;
  width: 100%;
  font-size: min(2rem, 5vw);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  :last-child {
    font-size: min(1.5rem, 4vw);
  }
`;

const Stage = styled.div`
  grid-area: stage;
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  padding: 30px;

  @media only screen and (max-width: 450px) {
    padding: 15px;
  }
  @media only screen and (max-width: 320px) {
    padding: 10px;
  }
`;

const Tag = styled.div<{ color: string }>`
  height: 2rem;
  width: 10rem;
  min-width: fit-content;
  font-size: min(1.2rem, 3.6vw);
  border-radius: 5px;
  background: ${(props) => props.color};
  display: grid;
  place-items: center;
  @media only screen and (max-width: 450px) {
    border-radius: 2px;
    font-size: min(0.7rem, 6vw);
    height: 1.5rem;
    width: 7rem;
  }
  @media only screen and (max-width: 320px) {
    border-radius: 1px;
    font-size: min(0.5rem, 4vw);
    height: 1rem;
    width: 5rem;
  }
`;

function selectStageColor(stage: Stages): string {
  if (stage === Stages.POSTMATCH) return 'gray';
  if (stage === Stages.PREMATCH) return 'green';
  return 'orange';
}

const selectBackground = (stage: Stages) => {
  if (stage === Stages.PREMATCH) return prematch;
  if (stage === Stages.POSTMATCH) return postmatch;
  return live;
};

function Card({ title, homeTeam, awayTeam, metadata }: MatchData) {
  return (
    <CardContainer background={selectBackground(metadata.stage)}>
      <Title>
        <h1>{title.main}</h1>
        <h2>{title.subtitle}</h2>
      </Title>
      <Team isHomeTeam={true}>
        <img src={homeTeam.flagURL} alt={`image of flag for ${homeTeam.name}`} />
        <p className={'sm-hide'}>{homeTeam.name}</p>
        <p className={'sm-show'}>{homeTeam.abbr}</p>
      </Team>
      {metadata.stage !== Stages.PREMATCH ? (
        <Score>{`${homeTeam.points}:${awayTeam.points}`}</Score>
      ) : (
        <Time>
          <p>VS</p>
          <p>{metadata.time}</p>
          <p>{metadata.date}</p>
        </Time>
      )}
      <Team isHomeTeam={false}>
        <img src={awayTeam.flagURL} alt={`image of flag for ${awayTeam.name}`} />
        <p className={'sm-hide'}>{awayTeam.name}</p>
        <p className={'sm-show'}>{awayTeam.abbr}</p>
      </Team>
      <Stage>
        <Tag color={selectStageColor(metadata.stage)}>{metadata.stage}</Tag>
      </Stage>
    </CardContainer>
  );
}

export default Card;
