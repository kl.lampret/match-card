import React, { useMemo } from 'react';
import Card from './Card';
import Carousel from './Carousel';
import { useMatches } from '../services/matchService';
import { getMatches } from '../utils/matchUtils';
import { LoadingCard } from './CardSkeleton';
import { ErrorCard } from './CardError';

interface MatchCarouselProps {
  sportId?: number;
  max?: number;
}

function MatchCarousel({ sportId, max = 10 }: MatchCarouselProps) {
  const { data, isLoading, isError } = useMatches();
  const matches = useMemo(() => getMatches(data, sportId), [data, sportId]);

  const itemsToDisplay = useMemo(() => {
    return matches?.slice(0, max) ?? [];
  }, [matches, max]);

  if (isLoading) return <LoadingCard />;
  if (isError) return <ErrorCard />;

  return (
    <>
      {matches ? (
        <Carousel
          items={itemsToDisplay}
          renderChild={(match) => (
            <Card
              title={match.title}
              homeTeam={match.homeTeam}
              awayTeam={match.awayTeam}
              metadata={match.metadata}
            />
          )}
        />
      ) : (
        <></>
      )}
    </>
  );
}

export default MatchCarousel;
