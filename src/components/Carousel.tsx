import React, { useState } from 'react';
import styled from 'styled-components';
import useInterval from '../hooks/useInterval';

type CarouselProps<T> = {
  items: Array<T>;
  renderChild: (item: T) => React.ReactNode;
  autoplayDelay?: number;
};

const Wrapper = styled.div`
  width: 100%;
`;

const Control = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
`;

const Button = styled.button<{ isactive: boolean }>`
  height: 15px;
  width: 15px;
  margin: 0 5px;
  border-radius: 50%;
  border: none;
  background: ${(props) => (props.isactive ? 'gray' : 'lightgray')};
`;

function Carousel<T>({ items, renderChild, autoplayDelay = 3000 }: CarouselProps<T>) {
  const [activeIndex, setActiveIndex] = useState(0);

  const resetTime = useInterval(() => {
    setActiveIndex((prevIndex) => (prevIndex + 1) % items.length);
  }, autoplayDelay);

  const handleButtonClick = (index: number) => {
    setActiveIndex(index);
    resetTime();
  };

  return (
    <Wrapper>
      {renderChild(items[activeIndex])}
      <Control>
        {items.map((_, index) => (
          <Button
            key={index}
            onClick={() => handleButtonClick(index)}
            isactive={index === activeIndex}
          ></Button>
        ))}
      </Control>
    </Wrapper>
  );
}

export default Carousel;
