import styled from 'styled-components';
import { UiCard } from './UiCard';

const Card = styled(UiCard)`
  background: red;
  display: grid;
  place-items: center;
`;

const Text = styled.p`
  color: white;
  font-size: 2rem;
`;

export function ErrorCard() {
  return (
    <Card>
      <Text>ERROR</Text>
    </Card>
  );
}
