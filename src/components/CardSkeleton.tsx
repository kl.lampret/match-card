import styled, { keyframes } from 'styled-components';
import { UiCard } from './UiCard';

const shimmer = keyframes`
  0% {
    background-position: -200% 0;
  }
  100% {
    background-position: 200% 0;
  }
`;

export const LoadingCard = styled(UiCard)`
  background: linear-gradient(90deg, #f0f0f0, #f8f8f8, #f0f0f0);
  background-size: 200% 100%;
  animation: ${shimmer} 1.5s infinite;
`;
