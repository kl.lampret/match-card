import styled from 'styled-components';

export const UiCard = styled.div`
  width: calc(100% - 20px);
  height: 450px;
  position: relative;
  box-sizing: border-box;
  margin: 20px 10px;
  padding: 20px;
  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
  border-radius: 16px;
  color: white;
  background: grey;

  @media only screen and (max-width: 450px) {
    height: 250px;
    margin: 10px 5px;
    padding: 10px;
    border-radius: 8px;
  }

  @media only screen and (max-width: 320px) {
    height: 170px;
    margin: 7px 3px;
    padding: 7px;
    border-radius: 5px;
  }
`;
