// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck

export interface ApiResponse {
  queryUrl: string;
  doc: Doc[];
}

interface Doc {
  event: string;
  _dob: number;
  _maxage: number;
  data: Daum[];
}

interface Daum {
  _doc: string;
  _id: number;
  _sid: number;
  name: string;
  realcategories: Realcategory[];
  live: boolean;
  _sk: boolean;
}

interface Realcategory {
  _doc: string;
  _id: number;
  _sid: number;
  _rcid: number;
  name: string;
  cc?: Cc;
  tournaments: Tournament[];
  _sk: boolean;
}

interface Cc {
  _doc: string;
  _id: number;
  a2: string;
  name: string;
  a3: string;
  ioc: string;
  continentid: number;
  continent: string;
  population: number;
}

interface Tournament {
  _doc: string;
  _id: number;
  _sid: number;
  _rcid: number;
  _isk: number;
  _tid: number;
  _utid: number;
  _gender?: string;
  name: string;
  abbr: string;
  ground?: Ground;
  friendly: boolean;
  seasonid?: number;
  currentseason?: number;
  year?: string;
  seasontype?: string;
  seasontypename?: string;
  seasontypeunique?: string;
  groupname?: string;
  livetable: any;
  cuprosterid?: string;
  roundbyround: boolean;
  tournamentlevelorder?: number;
  tournamentlevelname?: string;
  outdated: boolean;
  matches: Match[];
  _sk: boolean;
  tennisinfo?: Tennisinfo;
  scouts?: number;
}

interface Ground {
  _doc: string;
  _id: string;
  name: string;
  mainid: string;
  main: string;
}

export interface Match {
  _doc: string;
  _id: number;
  _sid: number;
  _seasonid?: number;
  _rcid: number;
  _tid: number;
  _utid: number;
  _dt: Dt;
  round?: number;
  roundname?: Roundname;
  week: number;
  coverage: Coverage;
  result: Result;
  periods?: Periods;
  updated_uts: number;
  ended_uts: any;
  p: string;
  ptime: any;
  timeinfo?: Timeinfo;
  teams: Teams;
  status: Status;
  removed: boolean;
  facts: boolean;
  stadiumid?: number;
  localderby: boolean;
  distance?: number;
  weather: any;
  pitchcondition: any;
  temperature: any;
  wind: any;
  windadvantage?: number;
  matchstatus: any;
  postponed: boolean;
  cancelled: boolean;
  walkover: boolean;
  hf: number;
  periodlength?: number;
  numberofperiods?: number;
  overtimelength?: number;
  tobeannounced: boolean;
  cards?: Cards;
  _sk: boolean;
  _mclink?: boolean;
  nextmatchid?: string;
  comment?: string;
  cuproundmatchnumber?: number;
  cuproundnumberofmatches?: number;
  basketballbonus?: Basketballbonus;
  livestate?: Livestate;
  penaltyshootout?: Penaltyshootout;
  bestofsets?: number;
  tiebreaks?: Tiebreaks;
  court?: Court;
  emptynet?: string;
  aggregate?: Aggregate;
  gamescore?: Gamescore;
}

interface Dt {
  _doc: string;
  time: string;
  date: string;
  tz: string;
  tzoffset: number;
  uts: number;
}

interface Roundname {
  _doc: string;
  _id: number;
  displaynumber: any;
  name: any;
  shortname?: string;
  cuproundnumber: any;
  statisticssortorder?: number;
}

interface Coverage {
  lineup: number;
  formations: number;
  livetable: number;
  injuries: number;
  ballspotting: boolean;
  cornersonly: boolean;
  multicast: boolean;
  scoutmatch: number;
  scoutcoveragestatus: number;
  scoutconnected: boolean;
  liveodds: boolean;
  deepercoverage: boolean;
  tacticallineup: boolean;
  basiclineup: boolean;
  hasstats: boolean;
  inlivescore: boolean;
  advantage?: number;
  tiebreak?: number;
  paperscorecard?: boolean;
  penaltyshootout: number;
  scouttest: boolean;
  lmtsupport: number;
  venue: boolean;
  matchdatacomplete: boolean;
  mediacoverage: boolean;
  substitutions: boolean;
}

interface Result {
  home?: number;
  away?: number;
  winner?: string;
}

interface Periods {
  p1: P1;
  p2?: P2;
  p3?: P3;
  ft?: Ft;
  p4?: P4;
  p5?: P5;
  ot?: Ot;
  ap?: Ap;
  p6?: P6;
  p7?: P7;
  p8?: P8;
  p9?: P9;
}

interface P1 {
  home: number;
  away: number;
}

interface P2 {
  home: number;
  away: number;
}

interface P3 {
  home: number;
  away: number;
}

interface Ft {
  home: number;
  away: number;
}

interface P4 {
  home: number;
  away: number;
}

interface P5 {
  home: number;
  away: number;
}

interface Ot {
  home: number;
  away: number;
}

interface Ap {
  home: number;
  away: number;
}

interface P6 {
  home: number;
  away: number;
}

interface P7 {
  home: number;
  away: number;
}

interface P8 {
  home: number;
  away: number;
}

interface P9 {
  home: number;
  away: number;
}

interface Timeinfo {
  injurytime: any;
  ended?: string;
  started?: string;
  played?: string;
  remaining?: string;
  running: boolean;
  suspensions?: Suspensions;
  suspensionsgiven?: Suspensionsgiven;
  suspensionplayers: any;
}

interface Suspensions {
  away?: string[];
  home?: string[];
}

interface Suspensionsgiven {
  away?: string[];
  home?: string[];
}

interface Teams {
  home: Home;
  away: Away;
}

interface Home {
  _doc: string;
  _id: number;
  _sid: number;
  uid: number;
  virtual: boolean;
  name: string;
  mediumname: string;
  abbr: string;
  nickname?: string;
  iscountry: boolean;
  haslogo: boolean;
  cc?: Cc2;
  surname?: string;
  seed?: Seed;
  children?: Children | undefined[];
}

interface Cc2 {
  _doc: string;
  _id: number;
  a2: string;
  name: string;
  a3: string;
  ioc?: string;
  continentid: number;
  continent?: string;
  population?: number;
}

interface Seed {
  seeding?: number;
  type_short?: string;
}

interface Children {
  _doc: string;
  _id: number;
  _sid: number;
  uid: number;
  virtual: boolean;
  name: string;
  mediumname: string;
  abbr: string;
  nickname: any;
  iscountry: boolean;
  haslogo: boolean;
  surname: string;
  cc?: Cc3;
}

interface Cc3 {
  _doc: string;
  _id: number;
  a2: string;
  name: string;
  a3: string;
  ioc?: string;
  continentid: number;
  continent?: string;
  population?: number;
}

interface Away {
  _doc: string;
  _id: number;
  _sid: number;
  uid: number;
  virtual: boolean;
  name: string;
  mediumname: string;
  abbr: string;
  nickname?: string;
  iscountry: boolean;
  haslogo: boolean;
  cc?: Cc4;
  surname?: string;
  seed?: Seed2;
  children?: Children2 | undefined[];
}

interface Cc4 {
  _doc: string;
  _id: number;
  a2: string;
  name: string;
  a3: string;
  ioc?: string;
  continentid: number;
  continent?: string;
  population?: number;
}

interface Seed2 {
  seeding?: number;
  type_short?: string;
}

interface Children2 {
  _doc: string;
  _id: number;
  _sid: number;
  uid: number;
  virtual: boolean;
  name: string;
  mediumname: string;
  abbr: string;
  nickname: any;
  iscountry: boolean;
  haslogo: boolean;
  surname: string;
  cc?: Cc5;
}

interface Cc5 {
  _doc: string;
  _id: number;
  a2: string;
  name: string;
  a3: string;
  ioc?: string;
  continentid: number;
  continent?: string;
  population?: number;
}

interface Status {
  _doc: string;
  _id: number;
  name: string;
}

interface Cards {
  home: Home2;
  away: Away2;
}

interface Home2 {
  yellow_count: number;
  red_count: number;
  yellow_during_half_time_count?: number;
  yellow_on_bench_count?: number;
  red_after_match_end_count?: number;
}

interface Away2 {
  yellow_count: number;
  red_count: number;
  yellow_on_bench_count?: number;
}

interface Basketballbonus {
  home: boolean;
  away: boolean;
}

interface Livestate {
  _doc: string;
  pitcher: Pitcher;
  batter: Batter;
  batting_average: any;
  inning_half: string;
  bases: Bases;
  batter_count: BatterCount;
  atbat: Atbat;
}

interface Pitcher {
  team: string;
  player: Player;
}

interface Player {
  _doc: string;
  _id: number;
  name: string;
  fullname?: string;
  birthdate: Birthdate;
  nationality?: Nationality;
  position: Position;
  primarypositiontype: string;
  haslogo: boolean;
}

interface Birthdate {
  _doc: string;
  time: string;
  date: string;
  tz: string;
  tzoffset: number;
  uts: number;
}

interface Nationality {
  _doc: string;
  _id: number;
  a2: string;
  name: string;
  a3: string;
  ioc: string;
  continentid: number;
  continent: string;
  population: number;
}

interface Position {
  _id: string;
  _type: string;
  name: string;
  shortname: string;
  abbr: string;
}

interface Batter {
  team: string;
  player: Player2;
}

interface Player2 {
  _doc: string;
  _id: number;
  name: string;
  birthdate: Birthdate2;
  position: Position2;
  primarypositiontype: string;
  haslogo: boolean;
  fullname?: string;
  nationality?: Nationality2;
}

interface Birthdate2 {
  _doc: string;
  time: string;
  date: string;
  tz: string;
  tzoffset: number;
  uts: number;
}

interface Position2 {
  _id: string;
  _type: string;
  name: string;
  shortname: string;
  abbr: string;
}

interface Nationality2 {
  _doc: string;
  _id: number;
  a2: string;
  name: string;
  a3: string;
  ioc: string;
  continentid: number;
  continent: string;
  population: number;
}

interface Bases {
  '1': N1;
  '2': N2;
  '3': N3;
}

interface N1 {
  occupied: boolean;
  player_id?: number;
}

interface N2 {
  occupied: boolean;
  player_id?: number;
}

interface N3 {
  occupied: boolean;
  player_id: any;
}

interface BatterCount {
  balls: number;
  strikes: number;
  outs: number;
  pitch_count: number;
  errors: number;
}

interface Atbat {
  number: number;
  pitchnumber: number;
}

interface Penaltyshootout {
  home: string;
  away: string;
}

interface Tiebreaks {
  p2?: P22;
  p1?: P12;
  p3?: P32;
}

interface P22 {
  home: number;
  away: number;
}

interface P12 {
  home: number;
  away: number;
}

interface P32 {
  home: number;
  away: number;
}

interface Court {
  _doc: string;
  _id: number;
  name: string;
}

interface Aggregate {
  home: number;
  away: number;
}

interface Gamescore {
  service: any;
  home?: number;
  away?: number;
}

interface Tennisinfo {
  name: string;
  city?: string;
  country?: string;
  cc?: Cc6;
  a2: string;
  a3: string;
  qualification: boolean;
  cityid?: string;
  gender: string;
  _gender: string;
  sets: number;
  start?: string;
  startTime?: StartTime;
  end?: string;
  endTime?: EndTime;
  prize: Prize;
  type?: string;
  _type?: string;
}

interface Cc6 {
  _doc: string;
  _id: number;
  a2: string;
  name: string;
  a3: string;
  ioc: string;
  continentid: number;
  continent: string;
  population: number;
}

interface StartTime {
  _doc: string;
  time: string;
  date: string;
  tz: string;
  tzoffset: number;
  uts: number;
}

interface EndTime {
  _doc: string;
  time: string;
  date: string;
  tz: string;
  tzoffset: number;
  uts: number;
}

interface Prize {
  amount: string;
  currency?: string;
}
